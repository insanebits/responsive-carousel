/**
 * Author : Robertas Jasmontas
 * Email  : robertasjasmontas@gmail.com
 * Version: 1.0
 * Date   : 2013-07-30
 */

(function($){

    /**
     * Initializer for slider
     * @param instance
     * @param options
     */
    function init(instance, options)
    {
        item_count  = instance.find(options.node_selector).length;
        // width of whole slider including margins
        var node_width = parseInt(instance.find(options.node_selector).first().outerWidth(true));
        var visible_nodes = Math.floor( instance.width() / node_width);

        // instance data, different for each instance
        instance.data('visible_nodes', visible_nodes);
        instance.data('node_width', node_width);

        // button click events
        jQuery(options.left_navigation, instance).click(function(event){
            animateEx(instance, event, 'left', options);
        });

        jQuery(options.right_navigation, instance).click(function(event){
            animateEx(instance, event, 'right', options);
        });

        /* setup positions */
        var pos = 0;
        jQuery.each(jQuery(options.node_selector, instance), function(k,v){
            jQuery(v).css('left', pos+'px' );
            pos = pos + node_width;
        });
    }

    /* checks if animation is finished */
    function animating(instance){
        if (jQuery(':animated', instance).length)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    function animateEx(instance, event, direction, settings)
    {
        var node_width = instance.data('node_width');
        var visible_nodes = instance.data('visible_nodes');
        var list = jQuery(settings.node_selector, instance);

        if(animating(instance))
        {
            console.log('double click');
            return false;
        }


        // adding last element to begining before slide
        if (direction == 'right'){
            var first = list.first();
            var last = list.last();

            // placing last before first
            first.before(last);

            last.css('left', first.position().left - node_width);
        }

        /* slide all elements */
        list.each(function(k,v){

            //console.log(visible_nodes);
            if(jQuery(v).css('display') == 'none')
            {
                jQuery(v).css('display', 'block');
            }

            var position = jQuery(v).position().left;

            if (direction == 'right')
            {
                var updated_position = position + node_width;
            } else if (direction == 'left') {
                var updated_position = position - node_width;
            }

            jQuery(v).animate({left: updated_position}, settings.duration);
        });

        // if direction was left to avoid disapearing node changing after all animations finished
        list.promise().done(function(){
            if (direction == 'left'){
                var first = list.first();
                var last = list.last();

                // placing first after last
                last.after(first);

                first.css('left', last.position().left + node_width);
            }
        });

    }

    function resize(instance, settings)
    {
        //calculating visible nodes
        var node_width = parseInt(instance.find(settings.node_selector).first().outerWidth(true));
        var visible_nodes = Math.floor( instance.width() / node_width);

        // hiding not visible nodes
        jQuery(settings.node_selector, instance).each(function(k,v){

            console.log(visible_nodes);

            if((k) > visible_nodes)
            {
                jQuery(v).css('display', 'none');
            } else {
                jQuery(v).css('display', 'block');
            }
        });

        // updating instance data
        instance.data('visible_nodes', visible_nodes);
        instance.data('node_width', node_width);

    }

    $.fn.ResponsiveSlider = function(settings){
        // Extend default settings
        var opts = $.extend({}, $.fn.ResponsiveSlider.defaults, settings);

        return this.each(function(settings){
            var options = $.extend({}, opts, $(this).data());
            var $this = $(this);

            if(options.debug)
            {
                console.log('ResponsiveSlider initializing options:');
                $.each(options, function(k,v){
                    console.log(k, ':\'' + v + '\'');
                });

                //console.log('ResponsiveSlider items already loaded ', items_loaded);
            }

            init($this, options);

            // for debugging when browser is resized
            jQuery(window).resize(function(){
                resize($this, options);
            });
        });
    }

    // set default option values
    $.fn.ResponsiveSlider.defaults = {
        node_selector: '.carousel-item',
        left_navigation: '.left-arrow',
        right_navigation: '.right-arrow',
        debug: true,
        duration: 500
    }

})(jQuery);